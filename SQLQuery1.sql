Use AdventureWorksOLTP2008R2
Go
--1.1
select COUNT(*) from Sales.SalesPerson;

--1.2
select FirstName,LastName from Person.Person where FirstName LIKE 'B%';

--1.3
select FirstName,LAstName,JobTitle from 
HumanResources.Employee INNER JOIN Person.Person ON 
JobTitle='Design Engineer'OR 
JobTitle='Tool Designer' OR JobTitle='Marketing Assistant';

--1.4
select * from Production.Product;
select Name,Color,Weight from Production.Product where weight=(select max(Weight) from Production.Product);

--1.5
select Description,MaxQty from Sales.SpecialOffer;
select Description,ISNULL([MaxQty],0.0) AS MaxQty from Sales.SpecialOffer;

--1.6
select * from Sales.CurrencyRate;
select avg(EndOfDayRate) as 'AverageRate' from Sales.CurrencyRate
where FromCurrencyCode='USD' and ToCurrencyCode='GBP' and YEAR((CurrencyRateDate))=2005;

--1.7
select * from Person.Person;
select ROW_NUMBER() OVER(ORDER BY FirstName asc) AS ID, FirstName,LastName FROM Person.Person where FirstName LIKE'%ss%';

--1.8
select * from Sales.SalesPerson;

select BusinessEntityId,CommissionPct, 'Commission Band'= 
CASE 
when CommissionPct = 0 then 'band 0'
when CommissionPct > 0 and CommissionPct <= 0.01 then 'band 1'
when CommissionPct > 0.01 and CommissionPct <= 0.015 then 'band 2'
when CommissionPct > 0.015 then 'band 3'
END from Sales.SalesPerson;

--1.9
DECLARE @EmployeeID int=
(Select Person.Person.BusinessEntityID FROM Person.Person 
where FirstName='Ruth' AND LastName='EllerBrock'
AND PersonType='EM');

EXEC dbo.uspGetEmployeeManagers @BusinessEntityID=@EmployeeID;

--1.10
select * from Production.Product;
select * from Production.ProductInventory;

select Max(dbo.ufnGetStock(ProductId))as ProductId from Production.Product;

--2.0
select * from Sales.SalesOrderHeader;

--join
select * from Sales.Customer c left outer join Sales.SalesOrderHeader s 
ON c.CustomerId=s.SalesOrderID where s.SalesOrderNumber IS NULL;

--subquery
select * from Sales.Customer c where c.CustomerID in
(select CustomerID from Sales.SalesOrderHeader s where s.SalesOrderNumber IS NULL);

--CTE
WITH s AS
(select SalesOrderID from Sales.SalesOrderHeader)
select * from Sales.Customer c LEFT OUTER JOIN s 
ON c.CustomerID=s.SalesOrderID where s.SalesOrderID is NULL;

--EXISTS
select * from Sales.Customer c where EXISTS
(select * from Sales.SalesOrderHeader s where s.SalesOrderID IS NULL AND c.customerID=s.CustomerID);

select * from Purchasing.PurchaseOrderDetail;

select * from Purchasing.PurchaseOrderHeader

select * from Purchasing.ShipMethod

select * from Purchasing.ProductVendor

select * from Sales.SalesOrderHeader

--3.0
select TOP 5 s.AccountNumber,OrderDate from Sales.SalesOrderDetail sd JOIN Sales.SalesOrderHeader s
ON sd.SalesOrderID=s.SalesOrderID
GROUP BY s.TotalDue,s.AccountNumber,OrderDate
HAVING SUM(sd.LineTotal) > 70000 ORDER BY OrderDate DESC


--4.0
select * from Sales.SalesOrderDetail;
GO
create function
UserInput(@salesOrderID int, @CurrencyCode nvarchar(10), @date date)
RETURNS TABLE AS RETURN
(select OrderQty,ProductID,Unitprice*(
select EndOfdayRate FROM Sales.CurrencyRate where ModifiedDate=@date AND ToCurrencyCode=@CurrencyCode)
AS UnitPrice FROM Sales.SalesOrderDetail where SalesOrderID=@salesOrderID )
GO
select * from UserInput(43661,'AUD','2005-07-01');

--5.0
select * from Person.Person;

create procedure 
GetPersonName @NameFirst nvarchar(10)=NULL
AS
BEGIN
select * from Person.Person where FirstName=@NameFirst
END

select * from Person.Person
GetPersonName 'Gail'

--6.0
GO
create Trigger LimitPrices ON
Production.Product
FOR 
UPDATE AS IF UPDATE(ListPrice) 
BEGIN IF EXISTS(
select * from inserted i JOIN deleted d
ON i.ProductID=d.ProductID
where 
i.ListPrice>(d.ListPrice*1.5)
)
BEGIN RAISERROR('Price increased may not be greater than 15 percent.Transaction Failed',16,1)
ROLLBACK TRAN 
END
END 
GO


ALTER TRIGGER
Production.LimitPrices ON
Production.Product
FOR 
UPDATE
AS 
IF UPDATE(ListPrice) 
BEGIN IF EXISTS(
select * from inserted i JOIN deleted d
ON i.ProductID=d.ProductID
where 
i.ListPrice>(d.ListPrice*1.5)
)
BEGIN RAISERROR('Price increased may not be greater than 15 percent.Transaction Failed',16,1)
ROLLBACK TRAN 
END
END 
GO

select * from Production.Product;

update Production.Product set ListPrice=0.00 where ProductID=522;

